from datetime import datetime
from inspect import Attribute
import re
import csv
import logging
from google.cloud import bigquery, storage
from aux_functions import determine_load_configs, file_updated, table_exists

prefix_append = 'append/'
prefix_fullload = 'full_load/'
cs_upload = 'OBJECT_FINALIZE'
cs_delete = 'OBJECT_DELETE'
write_disposition_append = 'WRITE_APPEND'
write_disposition_empty = 'WRITE_EMPTY'

def bq_import(event, context):
    # Initiate client objects
    bq_client = bigquery.Client()
    gcs_client = storage.Client()
    
    print('Action: ' + str(event))
    print('Context Info: ' + str(context))
    
    # Extract File name and further meta information
    action_attributes = event['attributes']
    object_id = action_attributes['objectId'] 
    bucket_id = action_attributes['bucketId']
    write_disposition = None

    if prefix_append in object_id:
        prefix = prefix_append
        write_disposition = write_disposition_append
    elif prefix_fullload in object_id:
        prefix = prefix_fullload
        write_disposition = write_disposition_empty
    
    subfolder, file = object_id.replace(prefix, '').split('/')
    file_name, file_type = re.split('\.', file)
    table_name_bq = subfolder

    resource = context.resource
    resource_name = resource['name']
    project = re.split('/', resource_name)[1]
    table_id = f'{project}.imports_cloud_storage.{table_name_bq}'
    uri = f'gs://{bucket_id}/{object_id}'

    # Access storage bucket and blob
    bucket = storage.Bucket(gcs_client, bucket_id, user_project=project)
    print('File: ' + file)
    blob = bucket.get_blob(object_id)
    print('Blob: ' + str(blob))

    # Upload of new files or updates
    if action_attributes['eventType'] == cs_upload:
        # fully loaded tables
        if write_disposition == write_disposition_empty:
            if table_exists(table_id):
                # For updated files: delete BQ table and load new data from file
                bq_client.delete_table(table_id)
                if file_updated(action_attributes):
                    print(f'The file {file_name} is overwritten. Table {table_id} already exists and will be overwritten.')
                # If there are existing tables, overwrite the table with data of the new file
                else:
                    print(f'The file {file_name} is uploaded, but other files already exist. Table {table_id} already exists and will be overwritten.')
            
            # Create Table via API request (for new or updated files)
            job_config = determine_load_configs(blob, file_type, write_disposition)
            bq_client.load_table_from_uri(uri, table_id, job_config = job_config) 

        # partially loaded tables
        elif write_disposition == write_disposition_append:
            # if a file is updated in an append subfolder, re-build table on all existing files
            if file_updated(action_attributes):
                bq_client.delete_table(table_id)
                print(f'Table {table_id} already exists and will be overwritten.')         
                # re-build table based no all existing files
                blobs = bucket.list_blobs()
                for blob in blobs:
                    relative_path = prefix + subfolder
                    # match all remaining file blobs belonging to this table
                    if re.match(f'{relative_path}.*\.{file_type}$', blob.name):
                        uri = f'gs://{bucket_id}/{blob.name}'
                        job_config = determine_load_configs(blob, file_type, write_disposition)
                        # Create Table via API request
                        bq_client.load_table_from_uri(uri, table_id, job_config = job_config) 
            
            # if a new file is uploaded in the append subfolder
            else: 
                job_config = determine_load_configs(blob, file_type, write_disposition)
                bq_client.load_table_from_uri(uri, table_id, job_config = job_config) 
            
    # Only 'true' delete interactions (excluding updates)
    elif action_attributes['eventType'] == cs_delete and file_updated(action_attributes) == False:
        if table_exists(table_id):    
            bq_client.delete_table(table_id)
        
        relative_path = prefix + subfolder
        print("Relative path: " + relative_path)
        blobs_folder = (blob for blob in bucket.list_blobs() if re.match(f'{relative_path}.*\.{file_type}$', blob.name))

        # if file is deleted from an append folder, re-build table on remaining files
        if write_disposition == write_disposition_append:
            for blob in blobs_folder:
                uri = f'gs://{bucket_id}/{blob.name}'
                job_config = determine_load_configs(blob, file_type, write_disposition)
                # Create Table via API request
                bq_client.load_table_from_uri(uri, table_id, job_config = job_config) 
        
        # if write_disposition == write_disposition, check whether an older file still exists
        # if there is still a file in the folder, take the latest to create the table
        elif write_disposition == write_disposition_empty:
            date_max = datetime.timestamp(datetime(1900, 1, 1))
            blob_newest = None
            # for all blobs in the considered subfolder, select the newest
            for blob in blobs_folder:
                print("Blob.name: " + blob.name + ", blob.time_created: " + str(datetime.timestamp(blob.time_created)) + ", date_max: " + str(date_max))
                if datetime.timestamp(blob.time_created) > date_max:
                    blob_newest = blob
                    date_max = datetime.timestamp(blob.time_created)
            
            try: 
                print('Newest blob to be loaded to Big Query: ' + blob_newest.name)
            except AttributeError:
                print(f'No blob in folder {subfolder}. Table {table_id} is deleted.')
                
            if blob_newest != None:
                uri = f'gs://{bucket_id}/{blob_newest.name}'
                job_config = determine_load_configs(blob_newest, file_type, write_disposition)
                # Create Table via API request
                bq_client.load_table_from_uri(uri, table_id, job_config = job_config) 

    # Compute execution time
    latency = datetime.now()-datetime.strptime(action_attributes['eventTime'], '%Y-%m-%dT%H:%M:%S.%fZ')
    print('Latency: ' + str(latency))