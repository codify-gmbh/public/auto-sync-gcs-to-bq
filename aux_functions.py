from google.cloud import bigquery
import logging
import csv

def determine_load_configs(blob, file_type, write_disposition = None):
    job_config = None
    if file_type == 'csv':
        with blob.open() as csvfile:
            try:
                delimiter = csv.Sniffer().sniff(csvfile.read(1024)).delimiter
                logging.info('Identified CSV delimiter: ' + delimiter)
                    
            except Exception:
                # use tab as fallback delimiter tab, as Sniffer.sniff() is most likely to fail resolving this delimiter type
                delimiter = '\t' 
                logging.warning('Could not resolve delimiter. Assume fallback delimiter tab!')
                        
                    
        job_config = bigquery.LoadJobConfig(
            autodetect = True,
            source_format = bigquery.SourceFormat.CSV,
            field_delimiter = delimiter,
            write_disposition = write_disposition
            # schema_update_options: ALLOW_FIELD_ADDITION/ALLOW_FIELD_RELAXATION
        )

    elif file_type == 'json':
        job_config = bigquery.LoadJobConfig(
            source_format = bigquery.SourceFormat.NEWLINE_DELIMITED_JSON
        )
    elif file_type == 'avro':
        job_config = bigquery.LoadJobConfig(
            source_format = bigquery.SourceFormat.AVRO
        )
    elif file_type == 'parquet':
        job_config = bigquery.LoadJobConfig(
            source_format = bigquery.SourceFormat.PARQUET
        )
    elif file_type == 'orc':
        job_config = bigquery.LoadJobConfig(
            source_format = bigquery.SourceFormat.ORC
        )
    return job_config

def table_exists(table):
    bq_client = bigquery.Client()
    try: 
        bq_client.get_table(table)
        return True
    except Exception:
        return False

def file_updated(action_attributes):
    try:
        action_attributes['overwroteGeneration'] 
        return True
    except KeyError:
        try: 
            action_attributes['overwrittenByGeneration'] 
            return True
        except KeyError:
            return False