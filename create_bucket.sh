PROJECT_ID=YOUR-PROJECT
STORAGE_CLASS=NEARLINE
BUCKET_LOCATION=YOUR-LOCATION
BUCKET_NAME=YOUR-BUCKET-NAME
gsutil mb -p $PROJECT_ID -c $STORAGE_CLASS -l $BUCKET_LOCATION -b on gs://$BUCKET_NAME