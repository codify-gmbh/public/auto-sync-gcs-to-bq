# Live Streaming Cloud Storage to BigQuery

Lukas Ringlage
March 2022

[Source Code](https://gitlab.com/codify-gmbh/public/auto-sync-gcs-to-bq)

## Recap of the relevant tools

On the Google Cloud Platform (GCP), Cloud Storage and BigQuery are powerful and indispensable tools for each data engineer.

Cloud Storage is an object storage system that is typically used to store large amounts of data in buckets. It includes the functionality to define lifecycle policies on data and to manage data accessibility via IAM rules. Compared to other storage solutions on the cloud it is relatively cheap. Hence, Cloud Storage is typically used for back-ups or to safely store and share large amounts of data.

BigQuery is a scalable, analytical database (often called cloud data warehouse) on GCP which is used to store, transform and analyze large amounts of mostly structured data. It comes with a SQL interface to query the data and additional tools such as BigQuery ML for training machine learning models or BI Engine for sub-second query processing.

However, these two tools are not just valuable on their own. There is a symbiotic relationship between them. For example, it is popular to ingest raw unstructured data into Cloud Storage and then import some of the data for further analysis into BigQuery. Similarly, rarely used data is transfered from BigQuery to Cloud Storage into storage-efficient file formats (e.g. parquet) to save costs.

## Problem

Having the data in Cloud Storage, it is easy to load the compressed data as [external tables](https://cloud.google.com/bigquery/external-data-cloud-storage#table-types) into BigQuery tables via the BigQuery CLI, the Cloud Console  or various client libraries (see [this article](https://cloud.google.com/architecture/streaming-data-from-cloud-storage-into-bigquery-using-cloud-functions)). Yet, loading the data manually into BigQuery makes the work of developers and analysts cumbersome and invokes the risk of unintended inconsistencies between Cloud Storage and BigQuery.

Hence, there is a need for an application that automatically synchronizes BigQuery with Cloud Storage, such that BigQuery tables are generated whenever new files are uploaded to Cloud Storage and tables are deleted whenever a file is removed on Cloud Storage. This set-up clearly makes developer’s work more dynamic and less error prone. For example, when a huge number of files from external databases or systems should be integrated or analyzed on the GCP, the time savings are particularly large. Even, the proposed solution can be used for near real-time analysis.

## Set-up: Description

To achieve automatic streaming of the data from Cloud Storage to BigQuery, one needs a trigger mechanism that indicates when something has changed and the code executing the necessary transfer steps.

![Technical Design](./figures/sync_gcs_bq.png "Technical Design")

For the transfer, Google provides a managed computing service Cloud Function (*FaaS*) which enables the execution of customized code based on event triggers. As indicated in [this article](https://cloud.google.com/architecture/streaming-data-from-cloud-storage-into-bigquery-using-cloud-functions) on streaming data from Cloud Storage to BigQuery, Cloud Functions can be triggered based on Cloud Storage events such as **OBJECT\_FINALIZE** or **OBJECT\_DELETE** using Pub/Sub as a message queue under the hood. Pub/Sub is a particularly helpful intermediate tool if there are multiple subscribers (i.e. consumers) consuming messages queued in a topic.
Moreover, Pub/Sub has rich functionality when it comes to handling messages (particularly queueing options and acknowledgement policies).

## Set-Up: Implementation

Having described the technical set-up, we will now dive into the step-by-step instruction of the necessary cloud services. All code snippets and files are public available in this [this](https://gitlab.com/codify-gmbh/public/auto-sync-gcs-to-bq) Git repo.

First, set up a cloud storage bucket. Consider that the bucket name has to be globally unique:

```bash
gsutil mb -p PROJECT_ID -c STORAGE_CLASS -l BUCKET_LOCATION -b on gs://BUCKET_NAME
```

Usually, data engineers might wish to distinguish between full loads and partial loads. To accomplish this, we need to create two folders, e.g. "full_load" and "append", in the new bucket. As I will explain later on, this folder structure will help to load the Cloud Storage files as BigQuery tables with the proper **write_disposition**, i.e. to be able to distinguish the cases of full and partial loads.

Next, configure a cloud function consuming Pub/Sub messages from a Pub/Sub topic. As the Pub/Sub topic is created under the hood. The cloud function should be located in the same region as the cloud storage bucket:

```bash
gcloud functions deploy FUNCTION_NAME \
  --entry-point ENTRY_POINT  \
  --region REGION \
  --trigger-topic TOPIC_NAME \
  --runtime RUNTIME_VERSION \
  --source SOURCE_PATH
```

The Cloud Function itself can be implemented in various programming languages and represents the streaming logic depending on the received [Cloud Storage event](https://cloud.google.com/storage/docs/pubsub-notifications#events) and further aspects.

Within the Cloud Function, useful meta information on the considered Cloud Storage object is passed as two dictionary objects, let's name them **event** and **context**.

```python
def bq_import(event, context):
    ...
```

On the one hand, the dictionary **event** contains information about the event in Cloud Storage itself. It will be used to obtain the **objectId** containing the folder name indicating the **write_disposition** for the job configuration, the file name and the file type. Moreover, the name of the table created in BigQuery can be generated based on the filename for full loads or the subfolder name for appending loads.

```python
action_attributes = event['attributes']
object_id = action_attributes['objectId'] 
bucket_id = action_attributes['bucketId']
write_disposition = None
    
subfolder, file = object_id.replace(prefix, '').split('/')
file_name, file_type = re.split('\.', file)
table_name_bq = subfolder
```

On the other hand, the dictionary **object** provides meta information about the bucket, the message and the event.

```python
resource = context.resource
resource_name = resource['name']
project = re.split('/', resource_name)[1]
table_id = f'{project}.imports_cloud_storage.{table_name_bq}'
uri = f'gs://{bucket_id}/{object_id}'

# Access storage bucket and blob
bucket = storage.Bucket(gcs_client, bucket_id, user_project=project)
blob = bucket.get_blob(object_id)
```

At this point, all the necessary information needed for the load resp. deletion of the Big Query is gathered.

In the next step, the event type (**eventType**) is extracted from the meta information in order to distinguish between upload (including overwrites) and delete actions.
In order to account for different types of upload and delete actions, a detailed case distinction is important in order to properly represent the changes(load/delete/overwrite) in the Cloud Storage bucket.

> **Hint:** In order to keep the code readable, repeatedly used code snippets are separated from the main.py file as functions. As you can observe in the file aux_functions.py, the function **table_exists** checks, whether a certain table already exists in BigQuery. The function **file_updated** checks, whether the considered file has replaced an existing file or is new. Finally, the function **determine_load_configs** returns the an object of the class **bigquery.LoadJobConfig()** specifying the load settings of the function **bq_client.load_table_from_uri()**, such as **file_type**, **write_disposition**, **delimiter** (in case of csv-file), etc.

Primarily, the handling of the different write modes has to be considered. In order to be able to distinguish between append and full loads, a folder structure is introduced in the Cloud Storage bucket. Full load data files, i.e. data that should be separately loaded into a BigQuery table, are stored in a folder "fullload/" and files that should be collectively loaded into a single table are stored in a folder "append/". Within both directories, subfolders are introduced to to determine the table name for BigQuery and to structure files belonging to the same table. The required **write_disposition** is then set based on the folder in which the uploaded file has been put.

```python
if prefix_append in object_id:
    prefix = prefix_append
    write_disposition = write_disposition_append
elif prefix_fullload in object_id:
    prefix = prefix_fullload
    write_disposition = write_disposition_empty
```

First, we consider the handling of file uploads separately for full and partial data loads.

```python
if action_attributes['eventType'] == cs_upload:
    # fully loaded tables
    if write_disposition == write_disposition_empty:
        ...
    # partially loaded tables
    elif write_disposition == write_disposition_append:
        ...
```

For the case of full data loads, we first have to check whether a table exists in BigQuery to avoid a crash of the Cloud Function. If a file has been overwritten in Cloud Storage, the old table must be deleted and a new table has to be created based on the new file. If a new file is loaded additionally to existing file(s) in a subfolder, the existing table is deleted as well. Then, the table is recreated based on the new data file to ensure that the newest full load is available in BigQuery. If the table does not exist so far, the table is just created based on the new data file.

```python
if write_disposition == write_disposition_empty:
    if table_exists(table_id):
        # For updated files: delete BQ table and load new data from file
        bq_client.delete_table(table_id)
        if file_updated(action_attributes):
            print(f'The file {file_name} is overwritten. Table {table_id} already exists and will be overwritten.')
        # If there are existing tables, overwrite the table with data of the new file
        else:
            print(f'The file {file_name} is uploaded, but other files already exist. Table {table_id} already exists and will be overwritten.')

    # Create Table via API request (for new or updated files)
    job_config = determine_load_configs(blob, file_type, write_disposition)
    bq_client.load_table_from_uri(uri, table_id, job_config = job_config) 
```

For the case of partial data loads, a similar data streaming logic applies. If a single data file from a subfolder has been overwritten, the old table is deleted and a new table is created based on all existing, updated data files. If a new file is added to a subfolder, the data from the new file is appended to the corresponding BigQuery table. If the new file is the first one in the subfolder, a new table is created.

```python
elif write_disposition == write_disposition_append:
    # if a file is updated in an append subfolder, re-build table on all existing files
    if file_updated(action_attributes):
        bq_client.delete_table(table_id)
        print(f'Table {table_id} already exists and will be overwritten.')         
        # re-build table based no all existing files
        blobs = bucket.list_blobs()
        for blob in blobs:
            relative_path = prefix + subfolder
            # match all remaining file blobs belonging to this table
            if re.match(f'{relative_path}.*\.{file_type}$', blob.name):
                uri = f'gs://{bucket_id}/{blob.name}'
                job_config = determine_load_configs(blob, file_type, write_disposition)
                # Create Table via API request
                bq_client.load_table_from_uri(uri, table_id, job_config = job_config) 
            
    # if a new file is uploaded in the append subfolder
    else: 
        job_config = determine_load_configs(blob, file_type, write_disposition)
        bq_client.load_table_from_uri(uri, table_id, job_config = job_config) 
```

If a file is deleted in the Cloud Storage bucket, the existing table is deleted in BigQuery in a first step.

```python
elif action_attributes['eventType'] == cs_delete and file_updated(action_attributes) == False:
    if table_exists(table_id):    
        bq_client.delete_table(table_id)
```

> **Hint:** Whenever a file is overwritten in the considered Cloud Storage bucket, both event types **OBJECT_FINALIZE** and **OBJECT_DELETE** are triggered and propagated to the Cloud Function. As the case of overwriting existing data files is already handled in the previous part as special case of the event **OBJECT_FINALIZE**, the event **OBJECT_DELETE** can be ignored.

Depending on the write mode, a reduced or older version of the table might be created in the second step.
For the case of partial data loads, the preceding BigQuery table has to be truncated by the data included in the deleted file. This is accomplished by creating the table based on all remaining data files in the considered subfolder.

```python
if write_disposition == write_disposition_append:
    for blob in blobs_folder:
        uri = f'gs://{bucket_id}/{blob.name}'
        job_config = determine_load_configs(blob, file_type, write_disposition)
        bq_client.load_table_from_uri(uri, table_id, job_config = job_config) 
```

For the case of full data loads, the table is recreated based on the latest of the remaining full load versions in the subfolder(**blobs_folder**), if older versions of the full load exist.

```python
elif write_disposition == write_disposition_empty:
    date_max = datetime.timestamp(datetime(1900, 1, 1))
    blob_newest = None
    # for all blobs in the considered subfolder, select the newest
    for blob in blobs_folder:
        print("Blob.name: " + blob.name + ", blob.time_created: " + str(datetime.timestamp(blob.time_created)) + ", date_max: " + str(date_max))
        if datetime.timestamp(blob.time_created) > date_max:
            blob_newest = blob
            date_max = datetime.timestamp(blob.time_created)
            
    try: 
        print('Newest blob to be loaded to Big Query: ' + blob_newest.name)
    except AttributeError:
        print(f'No blob in folder {subfolder}. Table {table_id} is deleted.')
                
    if blob_newest != None:
        uri = f'gs://{bucket_id}/{blob_newest.name}'
        job_config = determine_load_configs(blob_newest, file_type, write_disposition)
        # Create Table via API request
        bq_client.load_table_from_uri(uri, table_id, job_config = job_config) 
```

## Monitoring & Error Handling

As indicated in the architecture graph above, the Pub/Sub topic can serve many subscription. For instance a subscription could be created in order to compute and track monitoring KPIs in combination with KPI-based user notifications.

Moreover, the error handling in the cloud function can be implemented in a flexible way. Loading errors or format detection errors can be tolerated to a certain level (e.g. via argument **schema_update_options**), retries can be configured and finally, when the files cannot be loaded in BigQuery, the errors can be written into another Pub/Sub topic. Further error handling can then be made based on the error messages. More detailed error strategies are left for further blog posts.

## Summary

The proposed tool set-up can help to easily automize data flows in companies and fills the gap to make data located in files available for SQL analysis in BigQuery.
Particularly, the set-up is intuitive and easily customizable due to the flexible folder structure in the Cloud Storage bucket. The proposed solution is particularly powerful, when data files are regularly and automatically generated by other data tools. In this case, the data transfer steps are performed automatically in real-time.
When data formats and configurations vary a lot, the cloud function is potentially able to automatically detect the characteristics of the corresponding data format. Nevertheless, when files with new data format are streamed for the first time via the Cloud Function, the maintainer should check if the streaming process has been completed correctly. Especially, the automatized schema and delimiter detection might be errorneous. In this context, proper error handling is a big plus.
